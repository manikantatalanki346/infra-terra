variable "aws_ami" {
    description  = "type of server"
    type         = string
    default      = "ami-052efd3df9dad4825"
}

variable "instance_type" {
    description = "type of instance"
    type = string
    default = "t2.micro"
  
}

variable "aws_region" {
    description = "region"
    type = string
    default = "us-east-1"

}

variable "vpc_id" {
    description = "vpc range"
    type = string
    default = "10.0.0.0/16"
  
}

variable "pub_sub" {
    description = "public subnet range"
    type = string
    default = "10.0.1.0/24"
  
}

variable "pri_sub" {
    description = "private subnet range"
    type = string
    default = "10.0.2.0/24"
  
}

variable "ingress_1" {
    description = "port range"
    type = string
    default = "443"
  
}

variable "ingress_2" {
    description = "port range"
    type = string
    default = "80"
  
}

variable "ingress_3" {
    description = "port range"
    type = string
    default = "22"
  
}

variable "access_key" {
  
}

variable "secret_key" {
  
}
