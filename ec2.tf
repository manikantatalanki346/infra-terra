resource "aws_instance" "oreo" {
  ami           = var.aws_ami
  instance_type = var.instance_type
  subnet_id = aws_subnet.pub.id
  security_groups =  [aws_security_group.SG.id]
  key_name = "ci"
  associate_public_ip_address = true

  tags = {
    Name = "oreo"
  }
}

